<?php

use PHPUnit\Framework\TestCase;
use stlswm\JsonObject\ClassMap;
use stlswm\JsonObject\Json;

require './Composer.php';

/**
 * Class UnMarshal
 */
class UnMarshal extends TestCase
{
    /**
     *
     */
    public function testUnMarshal()
    {
        $user = new MarshalObj();
        $user->name = '张三';
        $user->sex = '男';
        $user->age = 30;
        $job1 = new MarshalJob();
        $job1->company = '公司一';
        $job1->name = '工作一';
        $job2 = new MarshalJob();
        $job2->company = '公司二';
        $job2->name = '工作二';
        $user->jobs = [$job1, $job2];
        echo 'marshal:' . "\n";
        $jsonStr = Json::marshal($user);
        echo $jsonStr, "\n";
        echo 'unmarshal:' . "\n";
        $user = new MarshalObj();
        Json::unMarshal($jsonStr, $user);
        var_dump($user);
    }
}

/**
 * Class MarshalJob
 */
class MarshalJob extends ClassMap
{
    public $company = '';
    public $name = '';
}

/**
 * Class MarshalObj
 */
class MarshalObj extends ClassMap
{
    public function getClassMap(): array
    {
        return [
            'jobs' => [MarshalJob::class],
        ];
    }

    public $name;
    public $sex;
    public $age;
    public $jobs;
}